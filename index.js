let
  port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080,
  ip = process.env.IP || process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0';

const express = require('express');
const app = express();
const settings = require('./settings.json');

app.set('view engine', 'pug')
app.use(express.static('public'))

app.get('*', (req, res) => {
  console.log('render');
  res.render('index', {key: settings.key})
})


app.listen(port, ip);
